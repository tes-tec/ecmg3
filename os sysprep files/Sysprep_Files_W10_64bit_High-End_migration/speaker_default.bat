﻿@echo off

echo Windows Registry Editor Version 5.00 > t1.reg
echo.

Set str=hex(b):00,00,00,00,00,00,00,00
Set str_reg=HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\MMDevices\Audio\Render
Setlocal enabledelayedexpansion

rem List all value about Audio and set value to zero.
for /f "skip=1 delims=: tokens=1,*" %%i in ('reg query %str_reg%') do (

	echo [%%i] >> t1.reg
	call :level

)

rem Find the Speakers path and save it to t1.txt
for /f "skip=1 delims=: tokens=1,*" %%h in ('reg query %str_reg%') do (

	for /f "tokens=1 delims= " %%a in ('reg query %%h\Properties /f Speakers') do (
		echo %%a >> t1.txt
	)
)

rem Delete Speakers level value.
for /f "tokens=9 delims=\" %%i in ('findstr /L %str_reg% t1.txt') do (
		set str1=%%i
		echo [%str_reg%\!str1!] >> t1.reg
		Set str=-
		call :level
)

regedit /s t1.reg
del /q t1.reg
del /q t1.txt
goto :end

:level
	echo "Level:0"=%str% >> t1.reg
	echo "Level:1"=%str% >> t1.reg
	echo "Level:2"=%str% >> t1.reg
:end


