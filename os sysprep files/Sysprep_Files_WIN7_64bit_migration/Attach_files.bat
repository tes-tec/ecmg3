@echo off

echo.
echo Copying files
copy /y Cleanup.bat C:\Windows\System32
copy /y DeleteExplorer.reg C:\Windows\System32\Sysprep
copy /y extend_disk.txt C:\Windows\System32
copy /y remove_rpc.bat C:\Windows\System32
copy /y reseal_win7.bat C:\Windows\System32\Sysprep
copy /y sysprep.xml C:\Windows\System32\Sysprep
copy /y reseal_win7.lnk %userprofile%\desktop
echo.
pause