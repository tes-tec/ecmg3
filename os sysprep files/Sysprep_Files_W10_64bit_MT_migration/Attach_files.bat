@echo off

echo.
echo Copying files
copy /y Cleanup.bat C:\Windows\System32
copy /y DeleteExplorer.reg C:\Windows\System32\Sysprep
copy /y reseal_win10.bat C:\Windows\System32\Sysprep
copy /y sysprep.xml C:\Windows\System32\Sysprep
copy /y reseal_win10.lnk "%userprofile%\desktop"
echo.
pause