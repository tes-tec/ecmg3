@ECHO OFF

Rem This will delete browsing information stored in IE
regedit /s C:\Windows\System32\Sysprep\DeleteExplorer.reg

Rem delete link to batch file reseal_win10.bat
del /Q "%userprofile%\desktop\reseal_win10.lnk"

Rem This will prep system for OOBE on first boot
C:\Windows\System32\Sysprep\sysprep /generalize /oobe /shutdown /unattend:C:\Windows\System32\Sysprep\Sysprep.xml
