@echo off
setlocal EnableDelayedExpansion
rem Assemble the list of line numbers

set numbers=
set myguid=
mkdir c:\elobcd

bcdedit /enum all > c:\elobcd\bcdentry.txt

for /F "delims=:" %%a in ('findstr /I /N /C:"Windows Recovery Environment" C:\elobcd\bcdentry.txt') do (
   set /A before=%%a-3, after=%%a+3
   set "numbers=!numbers!!before!: !after!: "
)
rem Search for the lines
(for /F "tokens=1* delims=:" %%a in ('findstr /N "^" C:\elobcd\bcdentry.txt ^| findstr /B "%numbers%"') do ( 
	echo %%b) 
	
) > C:\elobcd\output.txt

FOR /f "tokens=2 delims= " %%a IN (C:\elobcd\output.txt) DO ( 
	REM ECHO %%a
   	set myguid=%%a
   	echo !myguid!
	goto PERFORM

	
)

:PERFORM
	if "%myguid%"=="" ( 
	
		goto EOF
	)else (
		
		goto PROCESS
	)
	
:PROCESS
	echo processing ...
	bcdedit /delete %myguid% /cleanup
	

:EOF
	rmdir /s /q c:\elobcd
	echo exiting ...

	