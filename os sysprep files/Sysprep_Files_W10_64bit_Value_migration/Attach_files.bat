@echo off

echo.
echo Copying files
copy /y Cleanup.bat C:\Windows\System32
copy /y DeleteExplorer.reg C:\Windows\System32\Sysprep
copy /y extend_disk.txt C:\Windows\System32
copy /y remove_rpc.bat C:\Windows\System32
copy /y reseal_win10.bat C:\Windows\System32\Sysprep
copy /y sysprep.xml C:\Windows\System32\Sysprep
copy /y reseal_win10.lnk "%userprofile%\desktop"
copy /y speaker_default.bat C:\Windows\System32
copy /y speaker_default.reg C:\Windows\System32\Sysprep
echo.
pause