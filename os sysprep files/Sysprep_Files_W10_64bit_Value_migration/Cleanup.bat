@echo off
diskpart /s extend_disk.txt
bcdedit /timeout 3
bcdedit /deletevalue {current} recoverysequence
BCDEdit /set {current} Recoveryenabled No
del /q C:\Windows\System32\Sysprep\reseal_win10.bat
del /Q C:\Windows\System32\Sysprep\sysprep.xml
del /q C:\Windows\System32\Sysprep\Sysprep_succeeded.tag
del /q C:\Windows\System32\remove_rpc.bat
del /q C:\Windows\System32\Sysprep\DeleteExplorer.reg
del /q C:\Windows\System32\extend_disk.txt
del /q C:\Windows\System32\Sysprep\speaker_default.reg
del /q C:\Windows\System32\Cleanup.bat
exit